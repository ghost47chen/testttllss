#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <openssl/rand.h>
#include <openssl/evp.h>
#include <openssl/engine.h>

#include "gmssl.h"

static char hextoasc(int xxc)
{
	xxc &= 0x0f;
	if (xxc < 0x0a)
		xxc += '0';
	else
		xxc += 0x37;
	return (char)xxc;
}

static char hexlowtoasc(int xxc)
{
	xxc &= 0x0f;
	if (xxc < 0x0a)
		xxc += '0';
	else
		xxc += 0x37;
	return (char)xxc;
}

static char hexhightoasc(int xxc)
{
	xxc &= 0xf0;
	xxc = xxc >> 4;
	if (xxc < 0x0a)
		xxc += '0';
	else
		xxc += 0x37;
	return (char)xxc;
}

static char asctohex(char ch1, char ch2)
{
	char ch;
	if (ch1 >= 'A')
		ch = (char)((ch1 - 0x37) << 4);
	else
		ch = (char)((ch1 - '0') << 4);
	if (ch2 >= 'A')
		ch |= ch2 - 0x37;
	else
		ch |= ch2 - '0';
	return ch;
}

static int aschex_to_bcdhex(char aschex[], int len, char bcdhex[])
{
	int i, j;

	if (len % 2 == 0)
		j = len / 2;
	else
		j = len / 2 + 1;

	for (i = 0; i < j; i++)
		bcdhex[i] = asctohex(aschex[2 * i], aschex[2 * i + 1]);

	return (j);
}

static int bcdhex_to_aschex(char bcdhex[], int len, char aschex[])
{
	int i;

	for (i = 0; i < len; i++)
	{
		aschex[2 * i] = hexhightoasc(bcdhex[i]);
		aschex[2 * i + 1] = hexlowtoasc(bcdhex[i]);
	}

	return (len * 2);
}

int InitSocket(char *ip, int port)
{
	if (NULL == ip || port < 0)
	{
		printf("InitSocket parameter\n");
		return -1;
	}
	int sock = 0;
	int ret = 0;
	struct sockaddr_in svraddr;
	int reuse = 1;
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0)
	{
		printf("sock failed errno[%d] error[%s]\n", errno, strerror(errno));
		return -1;
	}

	//reuse address
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

	memset(&svraddr, 0, sizeof(svraddr));
	svraddr.sin_family = AF_INET;
	svraddr.sin_port = htons(port);
	inet_pton(AF_INET, ip, &(svraddr.sin_addr));

	//bind
	if ((ret = bind(sock, (struct sockaddr *)&svraddr, sizeof(svraddr))) < 0)
	{
		printf("bind failed errno[%d] error[%s]\n", errno, strerror(errno));
		close(sock);
		return -1;
	}
	return sock;
}

//在SSL握手时，验证服务端证书时会被调用，res返回值为1则表示验证成功，否则为失败
static int verify_cb(int res, X509_STORE_CTX *xs)
{
	//printf("verify_cb res[%d]\n",res);
	if (res == 1)
	{
		return res;
	}
	printf("Error :: %s  res[%d]  errcode[%d]\n", X509_verify_cert_error_string(xs->error), res, xs->error);
	switch (xs->error)
	{
		case X509_V_ERR_UNABLE_TO_GET_CRL:
			printf(" NOT GET CRL!\n");
			return 1;
		default:
			break;
	}
	return res;
}


//callback should return 1 to indicate verification success and 0 to indicate verification failure
static int Verify_cert_chain_callback(X509_STORE_CTX *x509_ctx, void *arg)
{
	//printf("Verify_cert_chain_callback \n");
	return 1;
	/*
	   int ret = 0;
	   X509 *cert = NULL;
	   cert = x509_ctx->cert;
	   if (cert != NULL)
	   {
	   TUnionX509Cer pCertInfo;
	   if ((ret = UnionGetCertificateInfoFromX509Struct(cert,&pCertInfo))<0)
	   {
	   UnionUserErrLog("in Verify_cert_chain_callback::UnionGetCertificateInfoFromX509Struct failed .\n");
	   ret = 0;
	   goto endfree;
	   }
	   if(UnionIsOKOfCertLink(&pCertInfo) == 1)
	   {
	//UnionUserErrLog("in Verify_cert_chain_callback::UnionIsOKOfCertLink ok \n");
	ret = 1;
	goto endfree;
	}else
	{
	UnionUserErrLog("in Verify_cert_chain_callback::UnionIsOKOfCertLink failed \n");
	ret = 0;
	goto endfree;
	}

	}else
	{
	UnionUserErrLog("in Verify_cert_chain_callback::cert is null \n");
	return 0;
	}

endfree:
return ret;
	 */
}

int SSL_CTX_use_CAcertificate_ASN1(SSL_CTX *ctx, int len, const unsigned char *cert)
{
	X509_STORE *cert_store;
	X509 *x;
	int ret = 0;

	cert_store = SSL_CTX_get_cert_store(ctx);
	if (cert_store == NULL)
	{
		printf("in SSL_CTX_use_CAcertificate_ASN1::cert_store is null\n");
		return -1;
	}
	x = d2i_X509(NULL, &cert, (long)len);
	if (x == NULL)
	{
		printf("in SSL_CTX_use_CAcertificate_ASN1::d2i_X509 failed \n");
		return -1;
	}
	ret = X509_STORE_add_cert(cert_store, x);
	if (ret <= 0)
	{
		printf("in SSL_CTX_use_CAcertificate_ASN1::X509_STORE_add_cert failed \n");
		X509_free(x);
		return -1;
	}
	X509_free(x);
	return 0;
}

static SSL_CTX *g_sslctx = NULL;

int init_gm_ssl_ctx()
{

	char caHex[2048] = "3082015E30820101A0030201020203123456300C06082A811CCF5501837505003035310B300906035504060C02636E310F300D060355040B0C066B65796F75323115301306035504030C0C736D32526F6F745465737432301E170D3136303830343036343534385A170D3236303830323036343534385A3035310B300906035504060C02636E310F300D060355040B0C066B65796F75323115301306035504030C0C736D32526F6F7454657374323059301306072A8648CE3D020106082A811CCF5501822D034200040F2F5A30995C4B7AFBB3A7A91131B5F5ECD4CE3FE1E3B2252102468992CD372BE7977AE7FD199525142DEF9CC20B00AEE26587154287706D8DEAE3F634DB1ECE300C06082A811CCF5501837505000349003046022100DEA6DB79C32D2299E94A3E1DA1E09D5ED0017561ABCDC94723FCE35DF95FDDE9022100E97B5480131287AA2454FA574C288434971396CBEEFEB0322B2B5DF2309AA5B3";
	char signcertHex[2048] = "308201AC30820151A00302010202045A92B5C3300C06082A811CCF5501837505003035310B300906035504060C02636E310F300D060355040B0C066B65796F75323115301306035504030C0C736D32526F6F745465737432301E170D3138303232353133313032385A170D3238303130343133313032385A308183310B300906035504060C02636E310B300906035504080C026764310B300906035504070C02677A310B3009060355040A0C026B79310E300C060355040B0C056D79206F74310E300C060355040B0C056B65796F75311B301906092A864886F70D0109010C0C687A68406B65796F752E636E3110300E06035504030C076875616E677A683059301306072A8648CE3D020106082A811CCF5501822D0342000475BDA094A303EFDFEA911DF5D7A096A50325322340AB1B028B6EAB1BE972C9FB1074E6E42BF2D984C92A4225108BFC13F9E0BC54FC6CAEB7831BDCCF68CE68E6300C06082A811CCF550183750500034700304402200400822F836D53745ECD91A3EC7FD285744F2FAC4D419F2EA4A6577ECC53259C0220119285E7E71A0E5BC97A014223986C7E1C705D4D6386458E8A26AC33EA82FA90";
	char encCertHex[2048] = "308201B230820157A00302010202045A92B9C7300C06082A811CCF5501837505003035310B300906035504060C02636E310F300D060355040B0C066B65796F75323115301306035504030C0C736D32526F6F745465737432301E170D3138303232353133323733365A170D3238303130343133323733365A308189310B300906035504060C02636E310B300906035504080C026764310B300906035504070C02677A310B3009060355040A0C026B79310E300C060355040B0C056D79206F74310E300C060355040B0C056B65796F75311E301C06092A864886F70D0109010C0F687A68656E63406B65796F752E636E3113301106035504030C0A6875616E677A68656E633059301306072A8648CE3D020106082A811CCF5501822D03420004BDB1EA6C090E89FC2A7B958B06F443B7BABBDAD48514397C6054F6901493FF3EDB13AA8D244555F1CCC405EA727540F2C176D8D4DE8757A30CF10E00C196E53C300C06082A811CCF5501837505000347003044022064C1864EBE86A982A77DF948415D5C51F97657012AF2C7A4D873A6905BA0B9C7022078E9248ACC440D9F3A1256421F9DEFF404C64BCE37BA0E7CC239176B12758667";
	char signDHex[128] = "7F9E94D10405AD50BCE8FF5A8896EAA465D02837EF71C61D7D925E7D73AC2000";
	char signPKHex[512] = "75BDA094A303EFDFEA911DF5D7A096A50325322340AB1B028B6EAB1BE972C9FB1074E6E42BF2D984C92A4225108BFC13F9E0BC54FC6CAEB7831BDCCF68CE68E6";
	char encDHex[128] = "BDA696354A66E5CB4DC0B0D008BCA44FF071CC30C76B3E6410E542ABD0C25DEB";
	char encPKHex[512] = "BDB1EA6C090E89FC2A7B958B06F443B7BABBDAD48514397C6054F6901493FF3EDB13AA8D244555F1CCC405EA727540F2C176D8D4DE8757A30CF10E00C196E53C";

	unsigned char ca[2048] = {0};
	int calen = 0;
	calen = aschex_to_bcdhex(caHex, strlen(caHex), (char *)ca);

	unsigned char signcert[2048] = {0};
	int signcertlen = 0;
	signcertlen = aschex_to_bcdhex(signcertHex, strlen(signcertHex), (char *)signcert);

	unsigned char enccert[2048] = {0};
	int enccertlen = 0;
	enccertlen = aschex_to_bcdhex(encCertHex, strlen(encCertHex), (char *)enccert);

	SSL_library_init();
	SSL_load_error_strings();
	OpenSSL_add_all_algorithms();
	ERR_load_crypto_strings();

	SSL_CTX *ctx = NULL;
	if(g_sslctx == NULL)
	{
		ctx = SSL_CTX_new(SSLv23_server_method());
		if (ctx == NULL)
		{
			printf("in init_gm_ssl_ctx::SSL_CTX_new failed Error[%s] .\n", ERR_error_string(ERR_get_error(), NULL));
			return -1;
		}
	}else
		ctx = g_sslctx;
	// 要求校验对方证书
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb);
	//add for userdefined verychainfunc
	SSL_CTX_set_cert_verify_callback(ctx, Verify_cert_chain_callback, 0);

	int ret = 0;

	//加载CA
	if ((ret = SSL_CTX_use_CAcertificate_ASN1(ctx, calen, ca)) < 0)
	{
		printf("SSL_CTX_use_CAcertificate_ASN1 failed\n");
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(ctx);
		return -1;
	}

	//加载签名证书和密钥
	if (SSL_CTX_use_certificate_ASN1(ctx, signcertlen, signcert) < 0)
	{
		printf("SSL_CTX_use_certificate_ASN1 signcert failed\n");
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(ctx);
		return -1;
	}

	if (Union_SSL_CTX_use_SM2PrivateKey(ctx, 0, signDHex, signPKHex) <= 0)
	{
		printf("Union_SSL_CTX_use_SM2PrivateKey signkey failed\n");
		SSL_CTX_free(ctx);
		ERR_print_errors_fp(stderr);
		return -1;
	}

	//判定私钥是否正确
	if (!SSL_CTX_check_private_key(ctx))
	{
		printf("SSL_CTX_check_private_key failed  Error[%s] \n", ERR_error_string(ERR_get_error(), NULL));
		SSL_CTX_free(ctx);
		ERR_print_errors_fp(stderr);
		return -1;
	}

	//加载加密证书和私钥

	if (SSL_CTX_use_certificate_ASN1(ctx, enccertlen, enccert) < 0)
	{
		printf("SSL_CTX_use_certificate_ASN1 enccert failed\n");
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(ctx);
		return -1;
	}

	if (Union_SSL_CTX_use_SM2PrivateKey(ctx, 1, encDHex, encPKHex) <= 0)
	{
		printf("Union_SSL_CTX_use_SM2PrivateKey enckey failed\n");
		SSL_CTX_free(ctx);
		ERR_print_errors_fp(stderr);
		return -1;
	}

	//判定私钥是否正确
	if (!SSL_CTX_check_private_key(ctx))
	{
		printf("SSL_CTX_check_private_key failed  Error[%s] \n", ERR_error_string(ERR_get_error(), NULL));
		SSL_CTX_free(ctx);
		ERR_print_errors_fp(stderr);
		return -1;
	}
	if(g_sslctx == NULL)
		g_sslctx = ctx;
	return 0;
}

int init_rsa_ssl_ctx()
{


	char caHex[2048] = "308202E4308201CCA003020102020203E9300D06092A864886F70D01010405003035310B300906035504060C02636E31163014060355040A0C0D7273614341526F6F7454657374310E300C06035504030C056B65796F75301E170D3135303631353233303331345A170D3235303631323233303331345A3035310B300906035504060C02636E31163014060355040A0C0D7273614341526F6F7454657374310E300C06035504030C056B65796F7530820122300D06092A864886F70D01010105000382010F003082010A0282010100B6C6F807D4ECA1A9978E4353629D581AA8CAC004440D2EED1281C1CBE7CE6878F21152C69CD3FCC2D9A84A2EAA122927046B3CD6472EF19FD7B808B3DEFD9AD37E033A7CEFFE47924EAFAC3DEFA8A59E6DF1B9032E29363B73F617CCD57BCCB68930C2CCBEE056E96BB91C0E3F23EFECD5F6293CD0A7957D860380835D9F14149EC7438E09BC95ACFC1D8A2421C73E2A69BBBB74892ABB402B7C00E9D47E506C0D04FC3AC2E26B5ABF386ABB09D0727874E4481B51BD44081357645A6A4D9BD38D7F2F87A286CB68C3EB659325461AE2E47D61B3CCE25782D2D824EF2E66C37E455D3AD93651952D388A493BDB1429DB9880B0857D92303E74ADED0AE2D40EA90203010001300D06092A864886F70D010104050003820101006A2F757E661DBD12CE6A5AC50790CCD86FD87C947C29B902390651C90799FF3A9258D9395E8824191CF7391A4583592E1590538897AFD6EE1362206979AE566030FF86B878411E979F82CC79CF49726EB3D9E837BF3C9381C78C2C5B97E37535218C32BF8D63543207657A8E03235E5CFAE5A697F6520AEBDAEAC98EBEB0B86D26EF9578E4C8D04FD9C377C7EF7C09F34B70BCA14EDF6CE2C29C6698F647532FE3C5E29022074ECAEE4B9CDE2521663B93C9C3AC6B207F80161082EF1CD0A180B073319502532119AA8CF86000F50B29EE6500334C694FA806D6CA8608EEF7AAD6D2574CAC6CB5B01032D77BEA1B1E8BDD86904FD7CD86534B11CB2CF8DA670B";
	char vkHex[2048] = "3082025E020100028181009923C6934F7A0183049BDA099106379C829A32AA0E53366748FE328613A0300DD835CDE8AE8FE85C7C74591BC1914AD75D9A807358A373E45B71E5D0A09807DE7584CC8F49E816779892B59F26D885873FE3638AD6CB8D8BEBF4221ABEB5D91B6657266C68B72844C0EBBF7D3EAB3671740DF10E3E117367EA7B118BA92734B5020301000102818049526648F0E42F261E864A928B03E98B7FC2B9A04F848B074FDD19AEB565DCAC3C49A2A19636E3DFA85D02AF35F24D5DA97D08A4719BAD28F899A399CCD28DED393EB75DE5B8F297E8479B30188E50E4C028F2181AFC05BEC046B6CA754123DABF02235ABC63DCB350461B953966A6242412D2BF36E0FA342BFB54261D1FA63F024100C9E00F4A30C7A1AA798086972C55D454FAF1990FFE1819DEF8173A011BBD924A320AD4596C3BD2E0253E3006A280ED8E2843708A5D328ADC422EBFDE0780A167024100C232B31760A0F410EE6F2EA34E2B0276507FD4E3BF4A5A511FFCFA1ABCD4D1B0F9B695E791ADBD6540BA6A4B76D43CB798FEA808CD941949786FC2206D435B8302410092F58E8BE6D8DD22BA12CB67EFB987BA59785981DFEAF1A2749E8BF654987B8F91C07A77A2A8264A50326649DE30D73EBEA6E19D885D0BE36E36942C70EFBE0F024100B33B23BB61BD6B44ED2AC43A887C7D8B41049A31906D492A3281C33B46288D57EF40AADDCCC0335AEE009EF6033822C9B874E0152FD78DF71726DE5E1B39DD8D024100A7EAB88FB67F3F6E0FFC5BDD7F9CB1C2B45DE6A88CAB867FDACD2F7F4107C469E33458B027B674F31C683671158D781C66180AB2DE665CD84FDFC7C79C7930CF";
	char certHex[2048] = "3082023E30820126A00302010202045C98855B300D06092A864886F70D01010505003035310B300906035504060C02636E31163014060355040A0C0D7273614341526F6F7454657374310E300C06035504030C056B65796F75301E170D3139303332353037333830365A170D3239303133313037333830365A3011310F300D06035504030C067273615F313030819F300D06092A864886F70D010101050003818D00308189028181009923C6934F7A0183049BDA099106379C829A32AA0E53366748FE328613A0300DD835CDE8AE8FE85C7C74591BC1914AD75D9A807358A373E45B71E5D0A09807DE7584CC8F49E816779892B59F26D885873FE3638AD6CB8D8BEBF4221ABEB5D91B6657266C68B72844C0EBBF7D3EAB3671740DF10E3E117367EA7B118BA92734B50203010001300D06092A864886F70D010105050003820101001A5FB234B07900C75D7746ABD5905375666DE40C5100C6ABE393810D5C75A454F32BC454E2DF9E3D923A7BBFAEBA41F4BB2317B881638CB08CBF1CD44B84339B2F4D3C7A9D54E35C1D2A6A200C3EB73522104108FFE0C13B7310C69C77997050A87B417FCCDEDB3120711F18B40715E2B483A8FC5B9D902FEE34F303EF1D4DE68A28BDAE070FA04575C7535CB94190BDAFEFD288D6D521684A9BCF351D049AED5D87FC394F5D2EC0FE713227046D157D251D5BCDF90B3C105D0F956DDEEA066169F5E7B3C031170A1CB2A2A31FAC728DBC851077BE058C415882EAC36875203BF76300E44DD1CDC4F310BC5C2AF073BE9D6DD67F3EB68FCF233D5C3E35ACE320";

	unsigned char ca[2048] = {0};
	int calen = 0;
	unsigned char vk[2048] = {0};
	int vklen = 0;
	unsigned char cert[2048] = {0};
	int certlen = 0;

	calen = aschex_to_bcdhex(caHex, strlen(caHex), (char *)ca);
	vklen = aschex_to_bcdhex(vkHex, strlen(vkHex), (char *)vk);
	certlen = aschex_to_bcdhex(certHex, strlen(certHex), (char *)cert);

	SSL_library_init();
	SSL_load_error_strings();
	OpenSSL_add_all_algorithms();
	ERR_load_crypto_strings();

	SSL_CTX *ctx = NULL;
	if(g_sslctx == NULL)
	{
		ctx = SSL_CTX_new(SSLv23_server_method());
		if (ctx == NULL)
		{
			printf("in init_rsa_ssl_ctx::SSL_CTX_new failed Error[%s] .\n", ERR_error_string(ERR_get_error(), NULL));
			return -1;
		}
	}else
		ctx = g_sslctx;

	// 要求校验对方证书
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb);
	SSL_CTX_set_cert_verify_callback(ctx, Verify_cert_chain_callback, 0);

	int ret = 0;
	//加载CA
	if ((ret = SSL_CTX_use_CAcertificate_ASN1(ctx, calen, ca)) < 0)
	{
		printf("SSL_CTX_use_CAcertificate_ASN1 failed\n");
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(ctx);
		return -1;
	}

	//加载证书
	if (SSL_CTX_use_certificate_ASN1(ctx, certlen, cert) <= 0)
	{
		printf("in UnionSSLCtxLoadRSASvrCertKey::SSL_CTX_use_certificate_ASN1 failed!\n");
		return -1;
	}

	//加载私钥
	if (SSL_CTX_use_PrivateKey_ASN1(EVP_PKEY_RSA, ctx, vk, vklen) <= 0)
	{
		printf("in UnionSSLCtxLoadRSASvrCertKey::SSL_CTX_use_PrivateKey_ASN1 failed Error[%s].\n", ERR_error_string(ERR_get_error(), NULL));
		return -1;
	}

	//判定私钥是否正确
	if (!SSL_CTX_check_private_key(ctx))
	{
		printf("in UnionSSLCtxLoadRSASvrCertKey::SSL_CTX_check_private_key failed  Error[%s] \n", ERR_error_string(ERR_get_error(), NULL));
		return -1;
	}
	if(g_sslctx == NULL)
		g_sslctx = ctx;
	return 0;
}



static void ShowCerts(SSL * ssl)
{
	X509 *cert;
	char *line;

	cert = SSL_get_peer_certificate(ssl);
	if (cert != NULL) {
		printf("数字证书信息:\n");
		line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
		printf("证书: %s\n", line);
		free(line);
		line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
		printf("颁发者: %s\n", line);
		free(line);
		X509_free(cert);
	} 
	else
		printf("无证书信息！\n");
}
int main(int argc, char **argv)
{
	if (argc < 2)
	{
		printf("Usage port\n");
		return -1;
	}

	signal(SIGPIPE, SIG_IGN);

	int ret = 0;
	int socket = 0;
	int port = 0;

	port = atoi(argv[1]);
	socket = InitSocket("0.0.0.0", port);
	if (socket <= 0)
	{
		printf("InitSocket failed errno[%d] error[%s]\n", errno, strerror(errno));
		return -1;
	}

	listen(socket, 100);

	init_gm_ssl_ctx();

	init_rsa_ssl_ctx();





	SSL *m_ssl = NULL;

	struct sockaddr_in cliaddr;
	char cli_ip[16] = {0};
	int cli_port = 0;
	int accept_fd = 0;
	int cli_len = sizeof(cliaddr);
	char recvmsg[1024] = {0};
	char sendmsg[1024] = {0};
	int len = 0;

	while (1)
	{

		printf("wait accept...\n");
		accept_fd = accept(socket, (struct sockaddr *)&cliaddr, &cli_len);
		if (accept_fd <= 0)
		{
			printf("accept failed errno[%d] error[%s]\n", errno, strerror(errno));
			continue;
		}
		memset(cli_ip, 0, sizeof(cli_ip));
		inet_ntop(AF_INET, &(cliaddr.sin_addr), cli_ip, sizeof(cli_ip));
		cli_port = ntohs(cliaddr.sin_port);
		printf("accept from ip[%s] port[%d]\n", cli_ip, cli_port);

		m_ssl = SSL_new(g_sslctx);
		SSL_set_fd(m_ssl, accept_fd);

		if (SSL_accept(m_ssl) == -1)
		{
			printf("Accept failed accept_fd=[%d] Error[%s].\n", accept_fd, ERR_error_string(ERR_get_error(), NULL));
			continue;
		}
		printf("SSL_accept ok\n");
		ShowCerts(m_ssl);
		while (1)
		{
			memset(recvmsg, 0, sizeof(recvmsg));
			len = SSL_read(m_ssl, recvmsg, sizeof(recvmsg));
			if (len <= 0)
			{
				printf("SSL_read failed\n");
				SSL_shutdown(m_ssl);
				SSL_free(m_ssl);
				m_ssl = NULL;
				close(accept_fd);

				break;
			}
			else
			{
				printf("recvmsg[%s]\n", recvmsg);
				len = SSL_write(m_ssl, recvmsg, strlen(recvmsg));
			}
		}
	}
	close(socket);
	return 0;
}
