#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdarg.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <openssl/rand.h>
#include <openssl/evp.h>
#include <openssl/engine.h>

#include "gmssl.h"

static char hextoasc(int xxc)
{
	xxc &= 0x0f;
	if (xxc < 0x0a)
		xxc += '0';
	else
		xxc += 0x37;
	return (char)xxc;
}

static char hexlowtoasc(int xxc)
{
	xxc &= 0x0f;
	if (xxc < 0x0a)
		xxc += '0';
	else
		xxc += 0x37;
	return (char)xxc;
}

static char hexhightoasc(int xxc)
{
	xxc &= 0xf0;
	xxc = xxc >> 4;
	if (xxc < 0x0a)
		xxc += '0';
	else
		xxc += 0x37;
	return (char)xxc;
}

static char asctohex(char ch1, char ch2)
{
	char ch;
	if (ch1 >= 'A')
		ch = (char)((ch1 - 0x37) << 4);
	else
		ch = (char)((ch1 - '0') << 4);
	if (ch2 >= 'A')
		ch |= ch2 - 0x37;
	else
		ch |= ch2 - '0';
	return ch;
}

static int aschex_to_bcdhex(char aschex[], int len, char bcdhex[])
{
	int i, j;

	if (len % 2 == 0)
		j = len / 2;
	else
		j = len / 2 + 1;

	for (i = 0; i < j; i++)
		bcdhex[i] = asctohex(aschex[2 * i], aschex[2 * i + 1]);

	return (j);
}

static int bcdhex_to_aschex(char bcdhex[], int len, char aschex[])
{
	int i;

	for (i = 0; i < len; i++)
	{
		aschex[2 * i] = hexhightoasc(bcdhex[i]);
		aschex[2 * i + 1] = hexlowtoasc(bcdhex[i]);
	}

	return (len * 2);
}

//在SSL握手时，验证服务端证书时会被调用，res返回值为1则表示验证成功，否则为失败
static int verify_cb(int res, X509_STORE_CTX *xs)
{
	if (res == 1)
	{
		return res;
	}
	printf("Error :: %s  res[%d]  errcode[%d]\n", X509_verify_cert_error_string(xs->error), res, xs->error);
	switch (xs->error)
	{
		case X509_V_ERR_UNABLE_TO_GET_CRL:
			printf(" NOT GET CRL!\n");
			return 1;
		default:
			break;
	}
	return res;
}


static void ShowCerts(SSL * ssl)
{
	X509 *cert;
	char *line;

	cert = SSL_get_peer_certificate(ssl);
	if (cert != NULL) {
		printf("数字证书信息:\n");
		line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
		printf("证书: %s\n", line);
		free(line);
		line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
		printf("颁发者: %s\n", line);
		free(line);
		X509_free(cert);
	} 
	else
		printf("无证书信息！\n");
}

char pgUnionInputStr[8192 + 1];

char *UnionInput(char *fmt, ...)
{
	va_list args;
	int i;

	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);

	for (i = 0; i < sizeof(pgUnionInputStr) - 1;)
	{
		pgUnionInputStr[i] = getchar();
		if ((pgUnionInputStr[i] == 10) || (pgUnionInputStr[i] == 13) || (pgUnionInputStr[i] == '\n'))
		{
			if (i == 0)
				continue;
			else
				break;
		}
		else
			i++;
	}
	//scanf("%s",pgUnionInputStr);
	pgUnionInputStr[i] = 0;
	return (pgUnionInputStr);
}

int SSL_CTX_use_CAcertificate_ASN1(SSL_CTX *ctx, int len, const unsigned char *cert)
{
	X509_STORE *cert_store;
	X509 *x;
	int ret = 0;

	cert_store = SSL_CTX_get_cert_store(ctx);
	if (cert_store == NULL)
	{
		printf("in SSL_CTX_use_CAcertificate_ASN1::cert_store is null\n");
		return -1;
	}
	x = d2i_X509(NULL, &cert, (long)len);
	if (x == NULL)
	{
		printf("in SSL_CTX_use_CAcertificate_ASN1::d2i_X509 failed \n");
		return -1;
	}
	ret = X509_STORE_add_cert(cert_store, x);
	if (ret <= 0)
	{
		printf("in SSL_CTX_use_CAcertificate_ASN1::X509_STORE_add_cert failed \n");
		X509_free(x);
		return -1;
	}
	X509_free(x);
	return 0;
}

static SSL_CTX *g_sslctx = NULL;

int init_gm_ssl_ctx()
{
	if (g_sslctx)
		return 0;
	char caHex[2048] = "3082015E30820101A0030201020203123456300C06082A811CCF5501837505003035310B300906035504060C02636E310F300D060355040B0C066B65796F75323115301306035504030C0C736D32526F6F745465737432301E170D3136303830343036343534385A170D3236303830323036343534385A3035310B300906035504060C02636E310F300D060355040B0C066B65796F75323115301306035504030C0C736D32526F6F7454657374323059301306072A8648CE3D020106082A811CCF5501822D034200040F2F5A30995C4B7AFBB3A7A91131B5F5ECD4CE3FE1E3B2252102468992CD372BE7977AE7FD199525142DEF9CC20B00AEE26587154287706D8DEAE3F634DB1ECE300C06082A811CCF5501837505000349003046022100DEA6DB79C32D2299E94A3E1DA1E09D5ED0017561ABCDC94723FCE35DF95FDDE9022100E97B5480131287AA2454FA574C288434971396CBEEFEB0322B2B5DF2309AA5B3";
	char signcertHex[2048] = "3082013D3081E2A00302010202045C9865CA300C06082A811CCF5501837505003033310B300906035504060C02636E310E300C060355040B0C056B65796F753114301206035504030C0B736D32526F6F7454657374301E170D3139303332353035323332345A170D3239303133313035323332345A30173115301306035504030C0C736D325F73736C5F746573743059301306072A8648CE3D020106082A811CCF5501822D034200048C4EBF5D6E67096C56CE54C667FE7369801AC897B776B358D822EBD71386F38741CC5E8FC6DDF6A628629FC5180AF19798EBF02144A8C3160DBC80339D2438B9300C06082A811CCF5501837505000348003045022056738F51AAF07E9BC7957B898F53C44336C1C4E504238B2F6A40884BA29FCC9E022100A3C17B6B547F1C511CAAFC4E0E437FEC877D203377BFB311251D61067901477B";
	char encCertHex[2048] = "3082013C3081E2A00302010202045C986650300C06082A811CCF5501837505003033310B300906035504060C02636E310E300C060355040B0C056B65796F753114301206035504030C0B736D32526F6F7454657374301E170D3139303332353035323533375A170D3239303133313035323533375A30173115301306035504030C0C736D325F73736C5F746573743059301306072A8648CE3D020106082A811CCF5501822D034200047ED7E7C4BA400C9AE382EBEE57C43C38EC40B1FBC03AFDDFBCEB41179D9421BF4A37395D9864E8FA393ED184DC66CB1DA51A7C2C3F4A99B37533216AD170CD12300C06082A811CCF550183750500034700304402203C7051E11FED61940EB2F2E53F201086E16005A7518479C3F65BC8FE4475695D02203B508C7106C53D133CF5B74E8C423D1CF2D088C924486C813ECB7F52BEB21D5E";
	char signDHex[128] = "DEF09A31A93239828B12D4899D6890CC6495708148064C4051776D1DB91A0905";
	char signPKHex[512] = "8C4EBF5D6E67096C56CE54C667FE7369801AC897B776B358D822EBD71386F38741CC5E8FC6DDF6A628629FC5180AF19798EBF02144A8C3160DBC80339D2438B9";
	char encDHex[128] = "2B5ACCC58175A39553F184B7D466653402DFFD369D07B868636E37CC1BFEA95B";
	char encPKHex[512] = "7ED7E7C4BA400C9AE382EBEE57C43C38EC40B1FBC03AFDDFBCEB41179D9421BF4A37395D9864E8FA393ED184DC66CB1DA51A7C2C3F4A99B37533216AD170CD12";

	unsigned char ca[2048] = {0};
	int calen = 0;
	calen = aschex_to_bcdhex(caHex, strlen(caHex), (char *)ca);

	unsigned char signcert[2048] = {0};
	int signcertlen = 0;
	signcertlen = aschex_to_bcdhex(signcertHex, strlen(signcertHex), (char *)signcert);

	unsigned char enccert[2048] = {0};
	int enccertlen = 0;
	enccertlen = aschex_to_bcdhex(encCertHex, strlen(encCertHex), (char *)enccert);

	SSL_library_init();
	SSL_load_error_strings();
	OpenSSL_add_all_algorithms();
	ERR_load_crypto_strings();

	const SSL_METHOD *meth = NULL;
	SSL_CTX *ctx = NULL;

	meth = CNTLS_client_method();

	ctx = SSL_CTX_new(meth);
	if (!ctx)
	{
		printf("SSL_CTX_new failed\n");
		ERR_print_errors_fp(stderr);
		return -1;
	}

	//要求校验对方证书

	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER, verify_cb);
	SSL_CTX_set_cipher_list(ctx, "ECC-SM4-SM3");
//	SSL_CTX_set_cipher_list(ctx,"ECDH-SM4-SM3");
	SSL_CTX_set_mode(ctx, SSL_MODE_AUTO_RETRY);

	//加载 server-ca

	if (SSL_CTX_use_CAcertificate_ASN1(ctx, calen, ca) < 0)
	{
		printf("SSL_CTX_use_CAcertificate_ASN1 failed\n");
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(ctx);
		return -1;
	}

	//加载签名证书和密钥
	if (SSL_CTX_use_certificate_ASN1(ctx, signcertlen, signcert) < 0)
	{
		printf("SSL_CTX_use_certificate_ASN1 signcert failed\n");
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(ctx);
		return -1;
	}

	if (Union_SSL_CTX_use_SM2PrivateKey(ctx, 0, signDHex, signPKHex) <= 0)
	{
		printf("Union_SSL_CTX_use_SM2PrivateKey signkey failed\n");
		SSL_CTX_free(ctx);
		ERR_print_errors_fp(stderr);
		return -1;
	}

	//判定私钥是否正确
	if (!SSL_CTX_check_private_key(ctx))
	{
		printf("SSL_CTX_check_private_key failed  Error[%s] \n", ERR_error_string(ERR_get_error(), NULL));
		SSL_CTX_free(ctx);
		ERR_print_errors_fp(stderr);
		return -1;
	}

	//加载加密证书和私钥

	if (SSL_CTX_use_certificate_ASN1(ctx, enccertlen, enccert) < 0)
	{
		printf("SSL_CTX_use_certificate_ASN1 enccert failed\n");
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(ctx);
		return -1;
	}

	if (Union_SSL_CTX_use_SM2PrivateKey(ctx, 1, encDHex, encPKHex) <= 0)
	{
		printf("Union_SSL_CTX_use_SM2PrivateKey enckey failed\n");
		SSL_CTX_free(ctx);
		ERR_print_errors_fp(stderr);
		return -1;
	}

	//判定私钥是否正确
	if (!SSL_CTX_check_private_key(ctx))
	{
		printf("SSL_CTX_check_private_key failed  Error[%s] \n", ERR_error_string(ERR_get_error(), NULL));
		SSL_CTX_free(ctx);
		ERR_print_errors_fp(stderr);
		return -1;
	}
	g_sslctx = ctx;

	return 0;
}




int init_rsa_ssl_ctx()
{


	char caHex[2048] = "308202E4308201CCA003020102020203E9300D06092A864886F70D01010405003035310B300906035504060C02636E31163014060355040A0C0D7273614341526F6F7454657374310E300C06035504030C056B65796F75301E170D3135303631353233303331345A170D3235303631323233303331345A3035310B300906035504060C02636E31163014060355040A0C0D7273614341526F6F7454657374310E300C06035504030C056B65796F7530820122300D06092A864886F70D01010105000382010F003082010A0282010100B6C6F807D4ECA1A9978E4353629D581AA8CAC004440D2EED1281C1CBE7CE6878F21152C69CD3FCC2D9A84A2EAA122927046B3CD6472EF19FD7B808B3DEFD9AD37E033A7CEFFE47924EAFAC3DEFA8A59E6DF1B9032E29363B73F617CCD57BCCB68930C2CCBEE056E96BB91C0E3F23EFECD5F6293CD0A7957D860380835D9F14149EC7438E09BC95ACFC1D8A2421C73E2A69BBBB74892ABB402B7C00E9D47E506C0D04FC3AC2E26B5ABF386ABB09D0727874E4481B51BD44081357645A6A4D9BD38D7F2F87A286CB68C3EB659325461AE2E47D61B3CCE25782D2D824EF2E66C37E455D3AD93651952D388A493BDB1429DB9880B0857D92303E74ADED0AE2D40EA90203010001300D06092A864886F70D010104050003820101006A2F757E661DBD12CE6A5AC50790CCD86FD87C947C29B902390651C90799FF3A9258D9395E8824191CF7391A4583592E1590538897AFD6EE1362206979AE566030FF86B878411E979F82CC79CF49726EB3D9E837BF3C9381C78C2C5B97E37535218C32BF8D63543207657A8E03235E5CFAE5A697F6520AEBDAEAC98EBEB0B86D26EF9578E4C8D04FD9C377C7EF7C09F34B70BCA14EDF6CE2C29C6698F647532FE3C5E29022074ECAEE4B9CDE2521663B93C9C3AC6B207F80161082EF1CD0A180B073319502532119AA8CF86000F50B29EE6500334C694FA806D6CA8608EEF7AAD6D2574CAC6CB5B01032D77BEA1B1E8BDD86904FD7CD86534B11CB2CF8DA670B";
	char vkHex[2048] = "3082025C02010002818100962EEA397F766031AED0D8E64CB10A0840B1A54A751503EF1D42AD76064A87F53F10D27AE0B8C55AF220353BC6E6D45AAF018F975E6D96CD3CD69D749B17A8E3A984714081D6F70C9F09A5D3F7FF6E78D4E19FDDC87A7D06427BAE44DB3438F49910942147006C176CDCDCF53B4EE172049ABF5E1FE715560921C8A841411DEB0203010001028180142BA4E5A80ADC0C9030189185C75953966CC5886485380C0B60033010D43DC2607FFFE4E4FDABF3F50502614EEB7DA5D84F5B5BB720FCD9FFCBCB45F7F68B862378D873F54E3EBE681AF5025981B6C87A534C8A2FBAD36C2A043473EB14188A38E984578D5AA97B3E1B63736839389C6CA6F7852FD520A3E7D4ABE268E63291024100C45E5BC1F906338641A356B110F6CB3B438E06D383BF185B18AEB05770972B8E09D5A3C0038C945B22F862CE2A78205F108AEBAA58CDC1CD3E38C6DC5B8153ED024100C3CA1F207B9AF4D5377E026E9F7C09257421E58BFA039AE93ED8D83B06E97F3A315A8BD56078EFEFB8BE54D7E864B82F3EA6D3DF4E819BDDB47F9D222381AE370241009E950D492310ACFF99768D86458AE9299D2ECFB87547182649529A3AA34491FA0D5B28F8E264DF5292754165F9CA356A5073C04A3B993F0FBF561ED2CA5B31290240524D3BAA6EEB2372218291C928B77E2CC5559E65A9F22C03524CDB106D066859F0F72C02C52BF14866F3733CD34ECB71E8B73E27079C4A05F763118D9139691F02401FCB90E392ACACB2B9B9B73C4C420B062A5ACD304A385E4F6C69A0896E4AD90A1A15186986B01E0CE5F4C4237E175698B376FA85E9CAE6AA7944F7C038787767";
	char certHex[2048] = "308202423082012AA00302010202045C99D3B1300D06092A864886F70D01010505003035310B300906035504060C02636E31163014060355040A0C0D7273614341526F6F7454657374310E300C06035504030C056B65796F75301E170D3139303332363037323433355A170D3239303230313037323433355A30153113301106035504030C0A7273615F636C69656E7430819F300D06092A864886F70D010101050003818D0030818902818100962EEA397F766031AED0D8E64CB10A0840B1A54A751503EF1D42AD76064A87F53F10D27AE0B8C55AF220353BC6E6D45AAF018F975E6D96CD3CD69D749B17A8E3A984714081D6F70C9F09A5D3F7FF6E78D4E19FDDC87A7D06427BAE44DB3438F49910942147006C176CDCDCF53B4EE172049ABF5E1FE715560921C8A841411DEB0203010001300D06092A864886F70D010105050003820101008FA8F0D1B1F06037EC01AF598D1D4261F3B82E2F7FA1F629B7034BAB41470B803DA019D3F710AA6363C3026A164D8CE597602B52947C81FDD32B5BE0D32FCBBF6742ECAEDC89E68C804CF41D59F4D7182D3F73DE629F466A818BBD6C27E22A1649E44CB52D47E1884353C3EA6E6EDA603C3B5E634DECE70B9DF00E1410D11AF734FE848424FBB8B2E65A5A593BB3AB85A963FB4A503274E020E213E3C80C58CDE7E6D684CA19562232E91FDEAC348BAC2E42A74BBA117D677B08E3035073300B767BB4EC26466439ED6AAF585F8EF4D4815C591E6329EBD3C941DDF6DA0F1D4D53265E3698B559043F1BA0CFCF5BFB2C04C0AF57539FE23470C22E2A98116834";

	unsigned char ca[2048] = {0};
	int calen = 0;
	unsigned char vk[2048] = {0};
	int vklen = 0;
	unsigned char cert[2048] = {0};
	int certlen = 0;

	calen = aschex_to_bcdhex(caHex, strlen(caHex), (char *)ca);
	vklen = aschex_to_bcdhex(vkHex, strlen(vkHex), (char *)vk);
	certlen = aschex_to_bcdhex(certHex, strlen(certHex), (char *)cert);

	SSL_library_init();
	SSL_load_error_strings();
	OpenSSL_add_all_algorithms();
	ERR_load_crypto_strings();

	SSL_CTX *ctx = NULL;
	ctx = SSL_CTX_new(SSLv23_client_method());
	

	// 要求校验对方证书
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb);
	//加密方式  
	SSL_CTX_set_cipher_list(ctx, "AES256-SHA");  
	//处理握手多次  
	SSL_CTX_set_mode(ctx, SSL_MODE_AUTO_RETRY); 

	int ret = 0;
	//加载CA
	if ((ret = SSL_CTX_use_CAcertificate_ASN1(ctx, calen, ca)) < 0)
	{
		printf("in init_rsa_ssl_ctx::SSL_CTX_use_CAcertificate_ASN1 failed\n");
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(ctx);
		return -1;
	}

	//加载证书
	if (SSL_CTX_use_certificate_ASN1(ctx, certlen, cert) <= 0)
	{
		printf("in init_rsa_ssl_ctx::SSL_CTX_use_certificate_ASN1 failed!\n");
		return -1;
	}

	//加载私钥
	if (SSL_CTX_use_PrivateKey_ASN1(EVP_PKEY_RSA, ctx, vk, vklen) <= 0)
	{
		printf("in init_rsa_ssl_ctx::SSL_CTX_use_PrivateKey_ASN1 failed Error[%s].\n", ERR_error_string(ERR_get_error(), NULL));
		return -1;
	}

	//判定私钥是否正确
	if (!SSL_CTX_check_private_key(ctx))
	{
		printf("in UnionSSLCtxLoadRSASvrCertKey::SSL_CTX_check_private_key failed  Error[%s] \n", ERR_error_string(ERR_get_error(), NULL));
		return -1;
	}
	g_sslctx = ctx;
	return 0;
}


int main(int argc, char **argv)
{
	if (argc < 4)
	{
		printf("Usage ip port mode\n");
		return -1;
	}
	int sock = 0;
	char ip[16] = {0};
	int port = 0;
	int ret = 0;
	int mode = 0; //0-sm2 1-rsa

	strcpy(ip, argv[1]);
	port = atoi(argv[2]);
	mode = atoi(argv[3]);

	struct sockaddr_in svraddr;
	memset(&svraddr, 0, sizeof(svraddr));
	svraddr.sin_family = AF_INET;
	inet_pton(AF_INET, ip, &(svraddr.sin_addr));
	svraddr.sin_port = htons(port);

	sock = socket(AF_INET, SOCK_STREAM, 0);

	if ((ret = connect(sock, (struct sockaddr *)&svraddr, sizeof(svraddr))) < 0)
	{
		printf("connect failed errno[%d] error[%s]\n", errno, strerror(errno));
		close(sock);
		return -1;
	}
	printf("connect ok\n");
	
	if(mode == 0)
		init_gm_ssl_ctx();
	else
		init_rsa_ssl_ctx();

	SSL *m_ssl = NULL;

	m_ssl = SSL_new(g_sslctx);
	SSL_set_fd(m_ssl, sock);

	ret = SSL_connect(m_ssl);
	if (ret < 0)
	{
		printf("SSL_connect failed \n");
		goto normalexit;
	}
	printf("ssl connect ok \n");
	ShowCerts(m_ssl);

	char sendbuf[1024] = {0};
	char recvbuf[1024] = {0};
	char *ptr = NULL;
	int len = 0;

	while (1)
	{

		memset(sendbuf, 0, sizeof(sendbuf));
		memset(recvbuf, 0, sizeof(recvbuf));
		ptr = UnionInput("please input msg:");
		len = strlen(ptr);
		memcpy(sendbuf, ptr, len);

		len = SSL_write(m_ssl, sendbuf, len);  
		len = SSL_read(m_ssl,recvbuf, sizeof(recvbuf));
		if(len<=0)
		{
			printf("SSL_read failed");
			break;
		}
		printf("recvbuf::[%s]\n",recvbuf);
	}

normalexit:
	close(sock);
	SSL_shutdown(m_ssl);
	SSL_free(m_ssl);
	m_ssl = NULL;
	SSL_CTX_free(g_sslctx);
	g_sslctx = NULL;
	return 0;
}
