#ifndef GM_SSL_H
#define GM_SSL_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <openssl/ssl.h>

    /*
 功能:初始化密码机配置
 输入:
 hsmip:密码机IP
 hsmport:服务端口
 msgheadlen:指令消息头长度
*/

    void init_hsmengines(char *hsmip, int hsmport, int msgheadlen);

    const SSL_METHOD *CNTLS_client_method(void); //密码套件SSL_CTX_set_cipher_list(ctx, "ECC-SM4-SM3");

    /*
 功能:加载密码机RSA私钥
 输入:
   ctx: 参考SSL_CTX_use_PrivateKey_ASN1函数的ctx
   vkbyLMKHex:私钥密文(Hex)
    derpkHex:DER格式的公钥(Hex)
 返回: <=0 失败
   > 0 成功
   
*/
    int Union_SSL_CTX_use_HSM_PrivateKey(SSL_CTX *ctx,
                                         char *vkbyLMKHex, char *derpkHex);

    /*
 功能:加载密码机SM2私钥
 输入:
   ctx: 参考SSL_CTX_use_PrivateKey_ASN1函数的ctx
   encflag:加密标记,0--签名密钥,1--加密密钥 (注:国密ssl需两个用户证书,一个签名证书,一个加密证书)
   vkbyLMKHex:私钥密文(Hex)
    pkHex:SM2的公钥X|Y(128Hex)
 返回: <=0 失败
   > 0 成功
*/
    int Union_SSL_CTX_use_HSM_SM2PrivateKey(SSL_CTX *ctx, int encflag,
                                            char *vkbyLMKHex, char *pkHex);

    /*	
	功能：加载软算法SM2私钥明文
	输入：
		ctx: 参考SSL_CTX_use_PrivateKey_ASN1函数的ctx
		encflag:加密标记,0--签名密钥,1--加密密钥 (注:国密ssl需两个用户证书,一个签名证书,一个加密证书)
		vkHex:私钥明文(Hex)
		pkHex:SM2的公钥X|Y(128Hex)
*/
    int Union_SSL_CTX_use_SM2PrivateKey(SSL_CTX *ctx, int encflag, char *vkHex, char *pkHex);

#ifdef __cplusplus
}
#endif

#endif
